# base image
FROM node:20-alpine

# enable pnpm
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

# Set the working directory for the rest of the commands
USER node
RUN mkdir /home/node/app
WORKDIR /home/node/app

# Copy the package.json and pnpm-lock files to the working directory
COPY --chown=node:node package.json ./
COPY --chown=node:node pnpm-lock.yaml ./

# Install dependencies
RUN pnpm install --frozen-lockfile

# Copy the rest of the source code to the working directory
COPY --chown=node:node . .