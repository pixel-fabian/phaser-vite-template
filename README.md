# Phaser 3 Vite Template 🎮

Template for a [Phaser 3](https://phaser.io/) project using TypeScript and Vite.

## Features

- Basic phaser scenes
- [TypeScript](https://www.typescriptlang.org/) support
- [Vite](https://vitejs.dev/) as development and build tool
- [eslint](https://eslint.org/) and [prettier](https://prettier.io/) to format code

## Use with node.js

Have [node.js](https://nodejs.org) and [pnpm](https://pnpm.io/) installed.

1. Clone or download this repository
2. Install dependencies `pnpm i`

`pnpm dev` launches the vite development server on localhost with hot reloading.

`pnpm build` builds the application to `dist`.

`pnpm serve` locally preview production build.

## Use with docker

Have [docker](https://www.docker.com/) with [docker-compose](https://docs.docker.com/compose/install/) installed.

`docker-compose up` will build from `dev.Dockerfile` and start the vite development server with hot reloading.

## Project Structure

```
├── dist                 // production build
├── public/assets        // static files like images, fonts or sounds
├── src                  // TypeScript files
│  ├── scenes
│  │   ├── GameScene.ts  // gameplay
│  │   ├── LoadScene.ts  // boot game, load assets
│  │   ├── MenuScene.ts  // main menu
│  │   └── UiScene.ts    // runs on top of GameScene
│  ├── game.ts           // Phaser setup
│  └── style.css         // styles for html and canvas
└── index.html           // entry point
```

## Licence

[MIT](/LICENSE)
