import { SCENES, TEXTURES } from '../constants';
import LoadingBar from '../objects/LoadingBar';

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.LOAD,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {}

  preload(): void {
    // add text
    this.add.text(360, 225, 'Loading...', {
      fontFamily: 'sans-serif',
      color: '#fff',
    });
    // create loading bar
    const loadingBar = new LoadingBar(this, 255, 255, 300, 40);
    this.load.on('progress', (percentage: number) => {
      loadingBar.fillBar(percentage);
    });
    // load all textures
    this.load.spritesheet(
      TEXTURES.BUTTON_PLAY,
      '/assets/img/button_01_play.png',
      {
        frameWidth: 64,
        frameHeight: 32,
      },
    );
    // load all audio
  }

  create(): void {
    this.scene.start(SCENES.MENU);
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
