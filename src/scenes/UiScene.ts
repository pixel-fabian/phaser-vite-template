import { SCENES } from '../constants';

export default class UiScene extends Phaser.Scene {
  //private gameScene: Phaser.Scene;

  constructor() {
    super({
      key: SCENES.UI,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
    //this.gameScene = this.scene.get(SCENES.GAME);
  }

  preload(): void {}

  create(): void {
    this.createUi();
  }

  update(): void {}

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  private createUi() {
    this.add.text(0, 0, 'UIText', {
      fontFamily: 'sans-serif',
      color: '#fff',
      fontSize: '16px',
    });
  }
}
